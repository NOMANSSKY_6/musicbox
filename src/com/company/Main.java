package com.company;

public class Main {

    public static void main(String[] args) {
	Musicbox mb1 = new Musicbox();
	Track t1 = new Track(160,"video killed the radiostar");
	Track t2 = new Track(130,"Bellbottoms");
	Track t3 = new Track(90,"was he slow?(remix)");
	Disk d1 = new Disk("Best of 80s pop",t1,t2,t3);
	Disk d2 = new Disk("bob Dylan: Desire",t1,t2,t3);
	Player p1 = new Player(mb1);
	mb1.setPlayer(p1);
	mb1.addDisk(d1);
	mb1.addDisk(d2);
	mb1.getPlayer().loadDisk(d1);
	mb1.play(1);
		mb1.getSumofMusic();

    }
}
