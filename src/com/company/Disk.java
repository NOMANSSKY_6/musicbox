package com.company;

import java.util.ArrayList;
import java.util.List;

public class Disk {
    public Disk setMusicbox(Musicbox musicbox) {
        this.musicbox = musicbox;
        return this;
    }

    private Musicbox musicbox;
    private String titel;
    private List<Track> tracklist;
    private Track t1;
    private Track t2;
    private Track t3;
    private int discid;



    public Disk(String titel, Track t1, Track t2, Track t3) {
        this.t1= t1;
        this.t2= t2;
        this.t3= t3;
        this.titel = titel;
        this.tracklist = new ArrayList<>();
    }

    public Track getTrack(int i){
        return tracklist.get(i); //zugriff auf Liste funktioniert nicht
    }

    public Disk setTracklist(List<Track> tracklist) {
        this.tracklist = tracklist;
        return this;
    }

}
