package com.company;

public class Player {
    private Musicbox musicbox;
    private Disk disk;
    private static boolean loadedWithDisc = false;

    public static boolean isLoadedWithDisc() {
        return loadedWithDisc;
    }

    public static void setLoadedWithDisc(boolean loadedWithDisc) {
        Player.loadedWithDisc = loadedWithDisc;
    }

    public void loadDisk(Disk disc){
        this.disk = disc;
        loadedWithDisc= true;
    }

    public Player(Musicbox musicbox) {
        this.musicbox = musicbox;
    }
}
