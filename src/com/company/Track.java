package com.company;

public class Track {
    private int lenght;
    private String Name;

    public Track(int lenght, String name) {
        this.lenght = lenght;
        Name = name;
    }

    public int getLenght() {
        return lenght;
    }

    public Track setLenght(int lenght) {
        this.lenght = lenght;
        return this;
    }

    public String getName() {
        return Name;
    }

    public Track setName(String name) {
        Name = name;
        return this;
    }
}
